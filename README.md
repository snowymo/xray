# X-Ray: Screenshot Accessibility via Embedded Metadata

Screenshots are frequently shared on social media, via personal communications, and in academic papers. Unfortunately, existing screenshot tools strip away semantics useful for making the content accessible, leaving only pixels. For example, a screenshot of a table removes the structural information useful for conveying it. We introduce *X-Ray*, a system that captures and embeds the semantics of the underlying content into images. Using the *X-Ray* screenshot tool, semantic information is captured and stored in the Exif data of the resulting image, allowing it to ``tag along'' as the image is shared and reposted. We demonstrate that our approach retains accessibility for screen reader users via a study with five blind participants. More generally, our approach suggests a method for embedding accessibility metadata into otherwise inaccessible formats, enabling them to retain the more accessible representations that are present at capture time. 

## How to use

1. Place the images provided in `sample_images` inside the images directory of your SD card. Make sure the EXIF is not stripped (Facebook will strip, Google Drive will not).
2. Compile and run the app (just press run)
3. It should find the image in the images directory.
4. Click on the image with Talkback on.

### Limitations

The Andrid EXIF library cannot store more than 2000-3000 characters. This is not a limitation of the technology but the library. As a result, complex GUIs such as webpages will sometimes fail. Future releases will fix this with a better library such as EXIFTool.

## Architecture of the system

There are two components to the system

1. Screenshot capture
2. Screenshot presentation and rendering

The first is handled by an Accessibility Service (ChatHeadService.kt). This captures the screen contents as a JPEG, captures the Android View Hierarchy and stores it inside the EXIF data of the screenshot. 

The second is handled by ImagePreviewActivity.kt and XRayImageView.kt. This subclasses ImageView and implements the ExploreByTouchHelper interface. The inner class TouchHelper is responsible for maintaining the virtual children.

## JSON format

An example of the JSON format used to store screen data is available below

```
{
  "boundsInParent": {
    "bottom": 1920,
    "left": 0,
    "right": 1080,
    "top": 0
  },
  "children": [
    {
      "boundsInParent": {
        "bottom": 1794,
        "left": 0,
        "right": 1080,
        "top": 0
      },
      "children": [
        {
          "boundsInParent": {
            "bottom": 1794,
            "left": 0,
            "right": 1080,
            "top": 0
          },
          "children": [
            {
              "boundsInParent": {
                "bottom": 1794,
                "left": 0,
                "right": 1080,
                "top": 0
              },
              "children": [
                {
                  "boundsInParent": {
                    "bottom": 1794,
                    "left": 0,
                    "right": 1080,
                    "top": 0
                  },
                  "children": [
                    {
                      "boundsInParent": {
                        "bottom": 1794,
                        "left": 0,
                        "right": 1080,
                        "top": 0
                      },
                      "children": [
                        {
                          "boundsInParent": {
                            "bottom": 219,
                            "left": 0,
                            "right": 1080,
                            "top": 0
                          },
                          "children": [],
                          "className": "android.widget.TextView",
                          "description": "",
                          "text": "Vacation Destination"
                        },
                        {
                          "boundsInParent": {
                            "bottom": 920,
                            "left": 0,
                            "right": 1080,
                            "top": 219
                          },
                          "children": [],
                          "className": "android.widget.ImageView",
                          "description": "Picture of man on beach wearing sunglasses",
                          "text": ""
                        },
                        {
                          "boundsInParent": {
                            "bottom": 1794,
                            "left": 0,
                            "right": 1080,
                            "top": 920
                          },
                          "children": [],
                          "className": "android.widget.ImageView",
                          "description": "Three people swimming in the clear ocean",
                          "text": ""
                        }
                      ],
                      "className": "android.support.v7.widget.LinearLayoutCompat",
                      "description": "",
                      "text": ""
                    }
                  ],
                  "className": "android.widget.FrameLayout",
                  "description": "",
                  "text": ""
                }
              ],
              "className": "android.view.ViewGroup",
              "description": "",
              "text": ""
            }
          ],
          "className": "android.widget.FrameLayout",
          "description": "",
          "text": ""
        }
      ],
      "className": "android.widget.LinearLayout",
      "description": "",
      "text": ""
    },
    {
      "boundsInParent": {
        "bottom": 1920,
        "left": 0,
        "right": 1080,
        "top": 1794
      },
      "children": [],
      "className": "android.view.View",
      "description": "",
      "text": ""
    }
  ],
  "className": "android.widget.FrameLayout",
  "description": "",
  "text": ""
}
```