package edu.cmu.spareddy.xray

import android.util.Base64
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.zip.Deflater
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterOutputStream

class Compress {
    companion object {
        fun compressString(inputString: String): String {
            val stream = ByteArrayOutputStream()
            try {
                val deflater = Deflater(Deflater.BEST_COMPRESSION, false)
                val out = DeflaterOutputStream(stream, deflater)
                out.write(inputString.toByteArray(charset("UTF-16")))
                out.close()
            } catch (e: IOException) {
                throw AssertionError(e);
            }
            val bytes = stream.toByteArray()
            return Base64.encodeToString(bytes, Base64.DEFAULT)
        }

        fun decompressString(inputString: String): String {
            val bytes = Base64.decode(inputString.toByteArray(), Base64.DEFAULT)
            val stream = ByteArrayOutputStream()
            val infStream = InflaterOutputStream(stream)
            infStream.write(bytes)

            return stream.toString("UTF-16")
        }
    }
}