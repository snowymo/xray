package edu.cmu.spareddy.xray

import android.media.ExifInterface

object Util {
    // list of string type fields to use
    // for storing view hierarchy since
    // each field can only store about 1000 bytes
    val fieldsToUse = listOf(
        ExifInterface.TAG_SCENE_TYPE,
        ExifInterface.TAG_USER_COMMENT,
        ExifInterface.TAG_MAKER_NOTE,
        ExifInterface.TAG_IMAGE_DESCRIPTION,
        ExifInterface.TAG_COPYRIGHT,
        ExifInterface.TAG_DEVICE_SETTING_DESCRIPTION,
        ExifInterface.TAG_ARTIST,
        ExifInterface.TAG_FILE_SOURCE,
        ExifInterface.TAG_MAKE,
        ExifInterface.TAG_RELATED_SOUND_FILE
    )
}
