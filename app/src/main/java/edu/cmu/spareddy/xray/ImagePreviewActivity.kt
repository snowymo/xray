package edu.cmu.spareddy.xray

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import java.io.File

class ImagePreviewActivity: AppCompatActivity() {

    val tag = "XRay: ImagePreview"

    var imageView: XRayImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setTitle("X-Ray Image Viewer")
        setContentView(R.layout.activity_image_preview)

        val fileName = intent.getStringExtra("fileName")
        imageView = findViewById(R.id.image_preview)

        // load the file
        val dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "XRay")
        var imgFile = File(dir, fileName)

        if(imgFile.exists()) {
            val bitmap: Bitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
            imageView!!.setImageBitmap(bitmap)

            // read the EXIF
            val ef = ExifInterface(imgFile.absolutePath)

            var chunks = arrayListOf<String>()
            for(field in Util.fieldsToUse) {
                val chunk = ef.getAttribute(field)
                if(chunk != null) {
                    Log.i(tag, "$field---${chunk.length}")
                    chunks.add(chunk)
                }
            }

            val compressed = chunks.joinToString("")
            if(compressed != "") {
                val exif = Compress.decompressString(compressed)
                Log.i(tag, compressed)
                Log.i(tag, exif)
                Log.i(tag, "${exif.length} ${compressed.length}")
                imageView!!.setImageJSON(exif)
            }
            else {
                Toast.makeText(this, "No EXIF in image", Toast.LENGTH_LONG).show()
            }
        }
    }
}