package edu.cmu.spareddy.xray

import android.graphics.Rect
import android.util.Log
import com.google.gson.GsonBuilder

class VirtualView(val boundsInParent: Rect,
                  val text: String,
                  val description: String,
                  val className: String = "",
                  var children: ArrayList<VirtualView> = arrayListOf()) {

    companion object {
        private val gson = GsonBuilder().setPrettyPrinting().create()
    }

    fun getScaledContainingBox(viewIn: XRayImageView): Rect {
        val temp = this.boundsInParent

        val viewHeight = viewIn.measuredHeight
        val viewWidth  = viewIn.measuredWidth
        val rawHeight  = viewIn.drawable.intrinsicHeight
        val rawWidth  =  viewIn.drawable.intrinsicWidth


        val scaleFactor = if(rawHeight > viewHeight) {
            viewHeight.toFloat() / rawHeight
        }
        else {
            viewWidth.toFloat() / rawWidth
        }

        val displayWidth = (rawWidth * scaleFactor).toInt()
        val displayHeight = (rawHeight * scaleFactor).toInt()

        val leftGap = (viewWidth - displayWidth) / 2
        val topGap = (viewHeight - displayHeight) / 2

        val bounds = Rect(
            (temp.left * scaleFactor).toInt() + leftGap,
            (temp.top * scaleFactor).toInt() + topGap,
            (temp.right * scaleFactor).toInt() + leftGap,
            (temp.bottom * scaleFactor).toInt() + topGap
        )

        return bounds
    }

    fun intersects(x: Float, y: Float, viewIn: XRayImageView? = null): Boolean {

        if(viewIn == null) {
            // origin is top left, y increases down, x increases right
            return x >= this.boundsInParent.left
                    && y >= this.boundsInParent.top
                    && x <= this.boundsInParent.right
                    && y <= this.boundsInParent.bottom
        }
        else {
            val bounds = this.getScaledContainingBox(viewIn)
            return x >= bounds.left
                    && y >= bounds.top
                    && x <= bounds.right
                    && y <= bounds.bottom
        }
    }

    override fun toString(): String {
        return gson.toJson(this)
    }
}