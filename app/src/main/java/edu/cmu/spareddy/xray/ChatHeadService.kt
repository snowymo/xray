package edu.cmu.spareddy.xray

import android.accessibilityservice.AccessibilityService
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PixelFormat
import android.graphics.Rect
import android.media.ExifInterface
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.ImageView
import java.io.File
import com.google.gson.GsonBuilder


class ChatHeadService : AccessibilityService(), GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private val REQUEST_MEDIA_PROJECTION: Int = 1
    val tag = "XRay: ChatHeadService"

    companion object {
        var gotPermissions = false
        var mediaProjectionResultCode: Int? = null
        var data: Intent? = null
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        Log.i(tag, event?.toString() ?: "null event")
    }

    override fun onInterrupt() {

    }

    private var mWindowManager: WindowManager? = null
    private var mChatHeadView: View? = null
    private var gestureDetector: GestureDetector? = null

    override fun onCreate() {
        super.onCreate()

        Log.i(tag, "onCreate")

        //Inflate the chat head layout we created
        mChatHeadView = LayoutInflater.from(this).inflate(R.layout.layout_chat_head, null)

        //Add the view to the window.
        val params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        //Specify the chat head position
        params.gravity = Gravity.TOP or Gravity.LEFT        //Initially view will be added to top-left corner
        params.x = 0
        params.y = 1800

        //Add the view to the window
        mWindowManager = getSystemService(WINDOW_SERVICE) as WindowManager?
        mWindowManager!!.addView(mChatHeadView, params)

        // double tap on the image to take a screenshot
        gestureDetector = GestureDetector(this, this)
        gestureDetector!!.setOnDoubleTapListener(this)

        mChatHeadView?.setOnTouchListener { v, event ->
            gestureDetector?.onTouchEvent(event) ?: false
        }
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        Log.i(tag, "onServiceConnected")
    }

    // called when double tapped
    private fun captureScreenshot() {
        Log.i(tag, "captureScreenshot")
        if(gotPermissions) {
            Screenshotter.getInstance()
                .setSize(1080, 1920)
                .takeScreenshot(this, mediaProjectionResultCode!!, data) {
                    Log.d(tag, "haveScreenshot")
                    val folder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "XRay")
                    val name = "${System.currentTimeMillis()}.jpeg"
                    val path = File(folder, name)

                    // check if file exists first
                    if(folder.mkdirs()) {
                        Log.e(tag, "onActivityRequest/REQUEST_MEDIA_PROJECTION/OK/MKDIRS_XRAY_DIR")
                    }
                    if(!path.createNewFile()) {
                        Log.e(tag, "onActivityRequest/REQUEST_MEDIA_PROJECTION/OK/CREATE_FAIL")
                    }

                    var root: AccessibilityNodeInfo? = rootInActiveWindow
                    for(window in windows) {
                        val winRoot = window.root
                        if(winRoot != null){
                            // get the window with the largest boundsInParent
                            if(root == null) {
                                root = winRoot
                            }
                            else {
                                var rootBounds = Rect()
                                var winBounds = Rect()
                                root.getBoundsInScreen(rootBounds)
                                winRoot.getBoundsInScreen(winBounds)

                                if(rootBounds.height() * rootBounds.width() < winBounds.height() * winBounds.width()) {
                                    root.recycle()
                                    root = winRoot
                                }
                                else {
                                    winRoot.recycle()
                                }
                            }
                        }
                    }

                    // walk the dom and capture the metadata
                    var metadataForEXIF: String? = null
                    if(root != null) {
                        metadataForEXIF = this.encodeDOMAsJSON(root)
                        Log.i(tag , metadataForEXIF)
                        root.recycle()
                    }

                    val fOut = path.outputStream()
                    it.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                    Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK/WROTE_TO_DISK")

                    fOut.flush()
                    fOut.close()

                    Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK/DOM_EXPLORE")

                    if(metadataForEXIF != null) {
                        Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK/EXIF_WRITE")
                        Log.i(tag, metadataForEXIF)
                        val compressed = Compress.compressString(metadataForEXIF)
                        val chunks = compressed.chunked(1000)
                        val ef = ExifInterface(path.absolutePath)

                        // Android EXIF does not allow more than 1000 bytes in each string field
                        // So we split among multiple fields lmao
                        chunks.zip(Util.fieldsToUse) { chunk, field ->
                            Log.i(tag, "$field--${chunk.length}")
                            ef.setAttribute(field, chunk)
                        }
                        ef.saveAttributes()

                    }
                    else {
                        Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK/EXIF_NULL")
                    }
                }
        }
    }

    val gson = GsonBuilder().create()
    private fun encodeDOMAsJSON(root: AccessibilityNodeInfo): String? {
        val baseNode = encodeNode(root)
        return gson.toJson(baseNode)
    }

    private fun encodeNode(node: AccessibilityNodeInfo): VirtualView {
        val text = node.text?.toString() ?: ""
        val description = node.contentDescription?.toString() ?: ""
        val className = node.className?.toString() ?: ""
        var bounds = Rect()
        node.getBoundsInScreen(bounds)

        var elem = VirtualView(bounds, text, description, className)
        for(i in 0 until node.childCount) {
            val child = node.getChild(i)
            if(child != null) {
                elem.children.add(encodeNode(child))
                child.recycle()
            }
        }
        return elem
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mChatHeadView != null) mWindowManager!!.removeView(mChatHeadView)
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onDoubleTap(e: MotionEvent?): Boolean {
        Log.i(tag, "Double Tap")

        Handler().postDelayed({
            this.captureScreenshot()
        }, 2000)
        return true
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        return
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        return false
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        return false
    }
}