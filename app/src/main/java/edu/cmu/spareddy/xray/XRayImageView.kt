package edu.cmu.spareddy.xray

import android.content.Context
import android.graphics.Canvas
import android.graphics.Outline
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.support.v4.widget.ExploreByTouchHelper
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.widget.ImageView
import android.view.accessibility.AccessibilityNodeInfo
import com.google.gson.GsonBuilder

class XRayImageView(context: Context, attributeSet: AttributeSet? = null) : ImageView(context, attributeSet) {

    private val tag = "XRayImageView"

    // virtual hierarchy
    private val mTouchHelper = TouchHelper()
    private val mChildren = arrayListOf<VirtualView>()
    private var mSelectedView = ExploreByTouchHelper.INVALID_ID
    private var mLockAccessibilityDelegate = false

    init {
        ViewCompat.setAccessibilityDelegate(this, mTouchHelper)
        ViewCompat.setImportantForAccessibility(this, ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_YES)
        mLockAccessibilityDelegate = true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val rect = Rect()
        Log.i(tag, getClipBounds(rect).toString())
        Log.i(tag, rect.toString())
    }

    override fun setAccessibilityDelegate(delegate: View.AccessibilityDelegate?) {
        if (!mLockAccessibilityDelegate) {
            super.setAccessibilityDelegate(delegate)
        }
    }

    public override fun dispatchHoverEvent(event: MotionEvent): Boolean {
        Log.i(tag, "dispatchHoverEvent")
        if (mTouchHelper.dispatchHoverEvent(event)) {
            Log.i(tag, "dispatchHoverEvent/HANDLED")
            return true
        } else {
            Log.i(tag, "dispatchHoverEvent/PASSED")
            return super.dispatchHoverEvent(event)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        Log.i(tag, "onTouchEvent")
        return false // TODO: handle this case
    }

    private val gson = GsonBuilder().create()
    fun setImageJSON(data_: String) {

        val json = data_
        val data = gson.fromJson(json, VirtualView::class.java)

        // linearize the children
        linearizeTree(data, this@XRayImageView.mChildren)
    }

    private fun linearizeTree(node: VirtualView, accumulator: ArrayList<VirtualView>) {
        for(child in node.children) {
            if((child.text != "" || child.description != "")
                    && child.children.size == 0) {
                accumulator.add(child)
            }
            linearizeTree(child, accumulator)
        }
    }

    inner class TouchHelper: ExploreByTouchHelper(this@XRayImageView) {

        override fun getVirtualViewAt(x: Float, y: Float): Int {
            Log.i(tag, "getVirtualViewAt")
            var i = 0
            for (child in this@XRayImageView.mChildren) {
                if (child.intersects(x, y, this@XRayImageView)) {
                    Log.i(tag, "getVirtualViewAt/i")
                    return i
                }
                i++
            }
            Log.i(tag, "getVirtualViewAt/INVALID")
            return ExploreByTouchHelper.INVALID_ID
        }

        override fun getVisibleVirtualViews(virtualViewIds: MutableList<Int>?) {
            Log.i(tag, "getVisibleVirtualViews")
            if (virtualViewIds != null) {
                for (i in 0 until this@XRayImageView.mChildren.size) {
                    virtualViewIds.add(i)
                }
            }
        }

        override fun onPopulateEventForVirtualView(virtualViewId: Int, event: AccessibilityEvent) {
            Log.i(tag, "onPopulateEventForVirtualView")
            event.contentDescription = Integer.toString(virtualViewId)
        }

        override fun onPopulateNodeForVirtualView(
            virtualViewId: Int,
            node: AccessibilityNodeInfoCompat
        ) {
            val virtualView = this@XRayImageView.mChildren.get(virtualViewId)
            val bounds = virtualView.getScaledContainingBox(this@XRayImageView)

            node.setBoundsInParent(bounds)

            node.text = virtualView.text
            node.contentDescription = virtualView.description
            node.addAction(AccessibilityNodeInfoCompat.ACTION_CLICK)

            Log.i(tag, "onPopulateNodeForVirtualView")

            if (virtualViewId == mSelectedView && virtualViewId != ExploreByTouchHelper.INVALID_ID) {
                Log.i(tag, "onPopulateNodeForVirtualView/SET_SELECTION")
                node.isSelected = true
            }
        }

        override fun onPerformActionForVirtualView(virtualViewId: Int, action: Int, args: Bundle?): Boolean {
            Log.i(tag, "onPerformActionForVirtualView")
            when (action) {
                AccessibilityNodeInfo.ACTION_CLICK -> {
                    Log.i(tag, "onPerformActionForVirtualView/ACTION_CLICK")
                    if (virtualViewId >= 0 && virtualViewId < this@XRayImageView.mChildren.size) {
                        Log.i(tag, "onPerformActionForVirtualView/ACTION_CLICK/IF")
                        mSelectedView = virtualViewId
                    }
                    Log.i(tag, "onPerformActionForVirtualView/ACTION_CLICK/TRUE")
                    return true
                }
            }

            Log.i(tag, "onPerformActionForVirtualView/FALSE")
            return false
        }

    }
}