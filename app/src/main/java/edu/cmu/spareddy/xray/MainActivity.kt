package edu.cmu.spareddy.xray

import android.Manifest
import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.media.projection.MediaProjectionManager
import android.net.Uri
import android.provider.Settings.ACTION_MANAGE_OVERLAY_PERMISSION
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.os.Environment
import android.widget.*
import java.io.File


class MainActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    val tag = "XRay: MainActivity"
    var listView: ListView? = null
    var adapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i(tag, "onCreate")

        // initialize the list view
        listView = findViewById<ListView>(R.id.image_list)
        listView!!.onItemClickListener = this

        //Check if the application has draw over other apps permission or not?
        //This permission is by default available for API<23. But for API > 23
        //you have to ask for the permission in runtime.
        getScreenRecordPermission()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            val intent = Intent(
                ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
        }
    }

    var fileNames = arrayListOf<String>()
    override fun onResume() {
        super.onResume()
        fileNames.clear()
        fileNames.addAll(getListOfFilesInXRayDir())

        if(adapter == null) {
            adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileNames)
            listView!!.adapter = adapter
        }
        adapter!!.notifyDataSetChanged()
    }

    /**
     * OS passes token for screen record permissions
     */
    val REQUEST_MEDIA_PROJECTION = 1
    private fun getScreenRecordPermission() {
        Log.i(tag, "getScreenRecordPermission")
        findViewById<Button>(R.id.notify_me).setOnClickListener(View.OnClickListener {
           val mediaProjectionManager = getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
            startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(), REQUEST_MEDIA_PROJECTION)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            Log.i(tag, "onActivityResult/CODE_DRAW")
            //Check if the permission is granted or not.
            // Settings activity never returns proper value so instead check with following method
            if (Settings.canDrawOverlays(this)) {
                Log.i(tag , "onActivityResult/CAN_DRAW")
            } else {
                Toast.makeText(
                    this,
                    "Draw over other app permission not available. Closing the application",
                    Toast.LENGTH_SHORT
                ).show()
            }
            finish()
        }
        else if (requestCode == REQUEST_MEDIA_PROJECTION) {
            Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION")
            if (resultCode == Activity.RESULT_OK) {
                Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK")
                verifyStoragePermissions(this)
                Log.i(tag, "onActivityResult/REQUEST_MEDIA_PROJECTION/OK/PERM")
                ChatHeadService.gotPermissions = true
                ChatHeadService.mediaProjectionResultCode = resultCode
                ChatHeadService.data = data
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val fileName = listView!!.getItemAtPosition(position) as String
        Log.i(tag, fileName)
        var intent = Intent(this, ImagePreviewActivity::class.java)
        intent.putExtra("fileName", fileName)
        startActivity(intent)
    }

    private fun getListOfFilesInXRayDir(): ArrayList<String> {
        val dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "XRay")
        val files = dir.listFiles()
        var names = arrayListOf<String>()
        if(files != null) {
            for (i in 0 until files.size) {
                names.add(files[i].name)
            }
        }
        return names
    }

    private val REQUEST_EXTERNAL_STORAGE = 2
    private val PERMISSIONS_STORAGE = arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
    fun verifyStoragePermissions(activity: Activity) {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
        }
    }

    companion object {
        private val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084
    }
}